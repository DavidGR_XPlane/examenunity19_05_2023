using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class asteroidSpawner : MonoBehaviour
{
    public GameObject asteroide;
    int posY;
    int random;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(generateAsteroid());
    }

    IEnumerator generateAsteroid()
    {
        while (true)
        {
            GameObject spawnedAsteriod = Instantiate(asteroide);
            //spawnedAsteriod.transform.position = 
            //spawnedAsteriod.transform.eulerAngles = new Vector3(spawnedAsteriod.transform.eulerAngles.x + Random.Range(0,360), spawnedAsteriod.transf)
            random = Random.Range(0, 2);
            if (random == 1)
            {
                spawnedAsteriod.transform.position = new Vector2(Random.Range(-8, 10), -6.50f);
            }
            else
            {
                spawnedAsteriod.transform.position = new Vector2(Random.Range(-8, 10), 6.50f);
            }
            //spawnedAsteriod.transform.rotation = Quaternion.Euler(0, 0, Random.Range(0, 359));
            spawnedAsteriod.transform.Rotate(0, 0, Random.Range(0, 359));
            spawnedAsteriod.GetComponent<Rigidbody2D>().AddForce(spawnedAsteriod.transform.up * 300);
            //spawnedAsteriod.GetComponent<Rigidbody2D>().velocity = new Vector3(4, Random.Range(0,350));


            yield return new WaitForSeconds(4);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
