using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class asteroid : MonoBehaviour
{
    public GameObject asteroidFill;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //boundaries pantalla
        //boundaries pantalla
        //if (this.transform.position.y > 15.15f)
        //{
        //    Destroy(this.gameObject);
        //}
        //if (this.transform.position.y < -15.36f)
        //{
        //    Destroy(this.gameObject);
        //}
        //if (this.transform.position.x > 15.54f)
        //{
        //    Destroy(this.gameObject);
        //}
        //if (this.transform.position.x < -15.31f)
        //{
        //    Destroy(this.gameObject);
        //}
        if (this.transform.position.y > 5.15f)
        {
            this.transform.position = new Vector2(this.transform.position.x, -5.36f);
        }
        if (this.transform.position.y < -5.36f)
        {
            this.transform.position = new Vector2(this.transform.position.x, 5.10f);
        }
        if (this.transform.position.x > 8.54f)
        {
            this.transform.position = new Vector2(-8.31f, this.transform.position.y);
        }
        if (this.transform.position.x < -8.31f)
        {
            this.transform.position = new Vector2(8, this.transform.position.y);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Bala"))
        {
            Destroy(this.gameObject);
            GameObject asteroid1 = Instantiate(this.gameObject);
            asteroid1.transform.position = this.transform.position;
            asteroid1.transform.localScale = this.transform.localScale / 2;
            asteroid1.transform.Rotate(0, 0, Random.Range(0, 359));
            asteroid1.GetComponent<Rigidbody2D>().AddForce(this.transform.up * 400);
            GameObject asteroid2 = Instantiate(asteroidFill);
            asteroid2.transform.position = this.transform.position;
            asteroid2.transform.localScale = this.transform.localScale / 2;
            asteroid2.transform.Rotate(0, 0, Random.Range(0, 359));
            asteroid2.GetComponent<Rigidbody2D>().AddForce(this.transform.up * 300);
        }
    }

}
