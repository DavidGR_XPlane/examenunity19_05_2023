using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class shipController : MonoBehaviour
{
    Boolean cooldooown;
    public GameObject projectile;
    //delegates per vida
    public delegate void lowerHp();
    public List<ScriptableObject> scriptableObjectsList;
    public event lowerHp OnEnemyCollision;
    // vidaController
    public vidaController vidaController;

    // Start is called before the first frame update
    void Start()
    {
        cooldooown = false;
        this.GetComponent<shipController>().OnEnemyCollision += vidaController.lowerHp; 
    }


    IEnumerator cooldown(int sec)
    {
        yield return new WaitForSeconds(sec);
        cooldooown = false;
    }

    // Update is called once per frame
    void Update()
    {   
        //
        // controls
        //
        if (Input.GetKey(KeyCode.W))
        {
            this.GetComponent<Rigidbody2D>().AddForce(this.transform.up * 2);
        }
        if (Input.GetKey(KeyCode.S))
        {
            this.GetComponent<Rigidbody2D>().AddForce(-(this.transform.up * 2));
        }
        if (Input.GetKey(KeyCode.A))
        {
            this.transform.Rotate(0, 0, 0.5f);
        }
        if (Input.GetKey(KeyCode.D))
        {
            this.transform.Rotate(0, 0, -0.5f);
        }
        if (Input.GetKeyDown(KeyCode.Space) && cooldooown == false)
        {
            GameObject bala = Instantiate(projectile);
            bala.transform.position = this.transform.position;
            bala.transform.rotation = this.transform.rotation;
            bala.GetComponent<Rigidbody2D>().AddForce(this.transform.up * 900);
            cooldooown = true;
            StartCoroutine(cooldown(1));
        }
        //
        // sortir fora de pantalla
        ////
        if (this.transform.position.y > 5.15f)
        {
            this.transform.position = new Vector2(this.transform.position.x, -5.36f);
        }
        if (this.transform.position.y < -5.36f)
        {
            this.transform.position = new Vector2(this.transform.position.x, 5.10f);
        }
        if (this.transform.position.x > 8.54f)
        {
            this.transform.position = new Vector2(-8.31f, this.transform.position.y);
        }
        if (this.transform.position.x < -8.31f)
        {
            this.transform.position = new Vector2(8, this.transform.position.y);
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Asteroid"))
        {
            OnEnemyCollision?.Invoke();
            Debug.Log("game over");
        }
    }
}
