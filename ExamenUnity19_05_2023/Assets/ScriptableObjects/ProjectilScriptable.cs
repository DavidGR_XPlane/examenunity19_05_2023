using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ProjectilScriptable : ScriptableObject
{

    public float scale;
    public Color color;
    public int penetracio;
    public int fireRate;
    public int velocitat;

}
