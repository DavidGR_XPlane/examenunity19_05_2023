using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class vidaController : MonoBehaviour
{
    public GameObject ship;
    int health = 3;
    public GameObject heart0;
    public GameObject heart1;
    public GameObject heart2;

    // Start is called before the first frame update
    void Start()
    {

    }

    public void lowerHp()
    {
        health--;
        checkHp();
    }

    private void checkHp()
    {
        if (health == 2)
        {
            Destroy(heart2);
        }
        if (health == 1)
        {
            Destroy(heart1);
        }
        if (health == 0)
        {
            Destroy(heart0);
            Destroy(ship.gameObject);
            Debug.Log("GAME OVER");
            SceneManager.LoadScene("GameOver");
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
