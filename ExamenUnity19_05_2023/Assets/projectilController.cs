using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class projectilController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //boundaries pantalla
        if (this.transform.position.y > 5.15f)
        {
            Destroy(this.gameObject);
        }
        if (this.transform.position.y < -5.36f)
        {
            Destroy(this.gameObject);
        }
        if (this.transform.position.x > 8.54f)
        {
            Destroy(this.gameObject);
        }
        if (this.transform.position.x < -8.31f)
        {
            Destroy(this.gameObject);
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Asteroid"))
        {
            Destroy(this.gameObject);
        }
    }
}
